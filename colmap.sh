#!/bin/bash -ex

# Photogrammetry with COLMAP
## https://colmap.github.io/cli.html

[[ $# -ne 2 ]] && echo "Usage: $0  <images folder> <empty workspace folder>" && exit 1

IMAGES="$1"
WORKSPACE="$2"
COLMAP_BUILD=.
FROM=${FROM:-1}

[[ ! -x "$COLMAP_BUILD"/src/exe/automatic_reconstructor ]] && echo "!feature_extractor" && exit 2

# "$COLMAP_BUILD"/src/exe/automatic_reconstructor \
#                --workspace_path "$WORKSPACE" \
#                --image_path "$IMAGES" \
#                --data_type 'individual' \
#                --quality 'high' \
#                --single_camera 1 \
#                --sparse 1 \
#                --dense 1 \
#                --use_gpu 0

Step=1
if [ $FROM -le $Step ]; then
    echo Step $Step
    ## Run on CPUs (COLMAP only wants CUDA GPUs
    ## Assumes all images have EXIF data & were all taken using the same camera
    #
    ## num_threads is here only to try to use less memory
    "$COLMAP_BUILD"/src/exe/feature_extractor \
                   --image_path "$IMAGES" \
                   --database_path "$WORKSPACE"/db.db \
                   --use_gpu false \
                   --SiftCPUExtraction.num_threads 2 \
                   --ImageReader.single_camera 1
    ls -lh "$WORKSPACE"
fi

Step=2
if [ $FROM -le $Step ]; then
    echo Step $Step
    "$COLMAP_BUILD"/src/exe/exhaustive_matcher \
                   --database_path "$WORKSPACE"/db.db \
                   --SiftMatching.use_gpu 0
    ls -lh "$WORKSPACE"
fi

Step=3
if [ $FROM -le $Step ]; then
    echo Step $Step
    mkdir "$WORKSPACE"/sparse
    "$COLMAP_BUILD"/src/exe/mapper \
                   --database_path "$WORKSPACE"/db.db \
                   --image_path "$IMAGES" \
                   --export_path "$WORKSPACE"/sparse
    ls -lh "$WORKSPACE"/sparse
fi

Step=4
if [ $FROM -le $Step ]; then
    echo Step $Step
    mkdir "$WORKSPACE"/dense
    "$COLMAP_BUILD"/src/exe/image_undistorter \
                   --image_path "$IMAGES" \
                   --input_path "$WORKSPACE"/sparse/0 \
                   --output_path "$WORKSPACE"/dense \
                   --output_type COLMAP \
                   --max_image_size 2000
    ls -lh "$WORKSPACE"/dense
fi

# echo Step 5
# "$COLMAP_BUILD"/exe/dense_stereo \
#     --workspace_path $PROJECT_PATH/dense \
#     --workspace_format COLMAP \
#     --DenseStereo.geom_consistency true

Step=5
if [ $FROM -le $Step ]; then
    echo Step $Step
    "$COLMAP_BUILD"/src/exe/dense_fuser \
                   --workspace_path "$WORKSPACE"/dense \
                   --workspace_format COLMAP \
                   --input_type geometric \
                   --output_path "$WORKSPACE"/dense/fused.ply
    ls -lh "$WORKSPACE"/dense/fused.ply
fi

Step=6
if [ $FROM -le $Step ]; then
    echo Step $Step
    "$COLMAP_BUILD"/src/exe/dense_mesher \
                   --input_path "$WORKSPACE"/dense/fused.ply \
                   --output_path "$WORKSPACE"/dense/meshed.ply
    ls -lh "$WORKSPACE"/dense/meshed.ply
fi
