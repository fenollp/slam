# SLAM things

## Data collection
1. https://github.com/JakobEngel/dso
1. https://github.com/colmap/colmap
1. https://github.com/uzh-rpg/rpg_svo
1. [On data collection](https://github.com/uzh-rpg/rpg_svo/wiki/Obtaining-Best-Performance)
1. http://pointclouds.org/documentation/tutorials/using_kinfu_large_scale.php
1. https://github.com/googlecartographer/cartographer

## Storing
1. https://github.com/OctoMap/octomap

## Visualizing
1. https://github.com/googlecartographer/point_cloud_viewer
